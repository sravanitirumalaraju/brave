//
//  Constant.swift
//  Brave
//
//  Created by apple on 31/10/17.
//  Copyright © 2017 apple. All rights reserved.
//
//import Foundation
import UIKit

//var BaseUrl = "https://whispering-hollows-83638.herokuapp.com"       // developement url
var BaseUrl = "https://courageisaskill.com"                            // live url
var signInURL = BaseUrl + "/signUp"
var loginURL = BaseUrl + "/login"
var getCoursesURL = BaseUrl + "/api/GetAllCourseList"
//var getCourseDetailsURL = BaseUrl + "/api/GetCourseDetails/:courseId"
var getChallengesListURL = BaseUrl + "/api/GetChallengesListByCourse/"
var getUserDetailsURL = BaseUrl + "/api/EditUserDetails/"
var getUserEditedDetailsURL = BaseUrl + "/api/UpdateUser/"
var uploadProfileURL = BaseUrl + "/userProfilePicUpload/"
var challengePath = "https://d3btcp4i8nc67d.cloudfront.net/"
var updatePromoURL = BaseUrl + "/api/UpdateUserPromoCode/"
var challengeStatusURL = BaseUrl + "/api/addChallengeStatus"
var getChallengeStatus = BaseUrl + "/api/GetChallengeStatus/"
var updatePaymentURL = BaseUrl + "/api/UpdateUserPayment"
var getPaymentURL = BaseUrl + "/api/UserCoursePaymentDetails/"
var forgotURL = BaseUrl + "/forgetPassword"
var appRegisterationURL = BaseUrl + "/api/AppRegistration"
var deleteTokenURL = BaseUrl + "/api/DeleteRegisterTokenOnSignout"

struct AlertMessages{
    static let NO_INTERNET_TITLE = "Internet connection lost."
    static let NO_INTERNET_MESSAGE = "Please check your Wi-Fi or Cellular data Settings"
    static let INTERNET_ERROR_MESSAGE = "Unable to connect to internet, please check your internet settings"
    static let CONNECTION_ERROR = "Error occured when contacting server, please try after a while"
    
}

struct AppColors {
    static let APP_COLOR_PINK:UIColor = UIColor(rgb: 0xFF4081)
    static let APP_COLOR_BLACK:UIColor = UIColor(red: 58.0/255.0, green: 60.0/255.0, blue: 62.0/255.0, alpha: 1.0)
    static let APP_COLOR_GRAY:UIColor = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0)
    static let APP_COLOR_WHITE:UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
}

