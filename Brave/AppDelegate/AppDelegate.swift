//
//  AppDelegate.swift
//  Brave
//
//  Created by apple on 09/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import CoreData
//import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseAuth
//var appUIColor_First:UIColor = UIColor(rgb:0xFF4081)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {
//UNUserNotificationCenterDelegate
    var window: UIWindow?
    var navigationController = UINavigationController()
    var timer = Timer.init()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Thread.sleep(forTimeInterval: 2)
        //local notifications
//        registerForNotifications() //sravani
//        configureUserNotificationsCenter()
        
        movetohomeScreen()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
//        if UserDefaults.standard.object(forKey: "autoRemindNotifications") == nil {
//            UserDefaults.standard.set(true, forKey: "autoRemindNotifications")
//        }
        application.registerForRemoteNotifications()
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    func applicationWillTerminate(_ application: UIApplication) {
       // self.saveContext()
    }
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print("Token is here   \(String(describing: Messaging.messaging().fcmToken))")
        if UserDefaults.standard.object(forKey: "FCM_Token") == nil{
            UserDefaults.standard.set(Messaging.messaging().fcmToken, forKey: "FCM_Token")
        }else{
            let fcmSavedToken = UserDefaults.standard.value(forKey: "FCM_Token") as! String
            if fcmSavedToken == Messaging.messaging().fcmToken{
                print(fcmSavedToken)
            }else{
                UserDefaults.standard.set(Messaging.messaging().fcmToken, forKey: "FCM_Token")
            }
        }
//        print(UserDefaults.standard.value(forKey: "FCM_Token") as! String)
        // Pass device token to auth
        //        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
    }
    
    /*
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Brave")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    */
    func movetohomeScreen(){
        if let retrievedToken = UserDefaults.standard.value(forKey: "token") as? String{
            if retrievedToken != "" {
                print("Retrieved password is: \(retrievedToken)")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "challengeListController") as! ChallengeListController
                self.navigationController.pushViewController(initialViewController, animated: true)
                self.navigationController.isNavigationBarHidden = true
                self.window?.rootViewController = navigationController
            }
        } else {
            print("do nothing")
        }
    }
    /*
    //gautham
    func registerForNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else { return }
            print("Notification requests have been granted")
        }
    }
    
    func configureUserNotificationsCenter() {
        // Configure User Notification Center
        UNUserNotificationCenter.current().delegate = self
        
        // Define Actions
        let action = UNNotificationAction(identifier: "dontRemindAgain", title: "Don't Remind Again", options: [])
        
        // Define Category
        let category = UNNotificationCategory(identifier: "category", actions: [action], intentIdentifiers: [], options: [])
        
        // Register Category
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
    
    static func queueNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Brave"
        notificationContent.body = "It's been a while since you took up a challenge. Go on and take one now!"
        notificationContent.categoryIdentifier = "category"
        
        let notificationTrigger24 = UNTimeIntervalNotificationTrigger(timeInterval: (120), repeats: false)

//        let notificationTrigger24 = UNTimeIntervalNotificationTrigger(timeInterval: (24 * 60 * 60), repeats: false)
        let notificationTrigger48 = UNTimeIntervalNotificationTrigger(timeInterval: (240), repeats: true)

//        let notificationTrigger48 = UNTimeIntervalNotificationTrigger(timeInterval: (48 * 60 * 60), repeats: true)
        
        let notificationRequest24 = UNNotificationRequest(identifier: UUID().uuidString, content: notificationContent, trigger: notificationTrigger24)
        
        let notificationRequest48 = UNNotificationRequest(identifier: UUID().uuidString, content: notificationContent, trigger: notificationTrigger48)
        
        UNUserNotificationCenter.current().add(notificationRequest24) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
        
        UNUserNotificationCenter.current().add(notificationRequest48) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "dontRemindAgain":
            UserDefaults.standard.set(false, forKey: "autoRemindNotifications")
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        default:
            print("Other Action")
        }
        completionHandler()
    }
 */
}

