//
//  ChallengeCollectionCell.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import Foundation

class ChallengeCollectionCell: UICollectionViewCell {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var itemTitle: BraveUILabel!
    @IBOutlet var noDataLabel: UILabel!
    
    override func awakeFromNib() {
       containerView.makeCircular()
        
    }
    func showSelectionOnCell(){
        containerView.backgroundColor =  AppColors.APP_COLOR_PINK
        itemTitle.textColor = UIColor.white
        
    }
    
}
