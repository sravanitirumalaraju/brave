//
//  BraveUILabel.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation
import UIKit


class BraveUILabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
        numberOfLines = 0
        lineBreakMode = NSLineBreakMode.byWordWrapping
        adjustsFontSizeToFitWidth =  true
        if #available(iOS 9.0, *) {
            allowsDefaultTighteningForTruncation =  true
        } else {
            // Fallback on earlier versions
        }
        sizeToFit()
    }
    
    func changeFontName()
    {
        self.font = UIFont(name: "Helvetica", size: self.font.pointSize)
    }
}

