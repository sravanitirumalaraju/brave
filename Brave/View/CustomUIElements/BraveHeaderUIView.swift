//
//  BraveHeaderUIView.swift
//  Brave
//
//  Created by apple on 11/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation
import UIKit

class BraveHeaderUIView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        //custom initialization
        //setCardView(view: self)
        addShadow()
    }
    
    override func updateConstraints() {
        //set subview constraints here
        super.updateConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //manually set subview frames here
    }
    
}
