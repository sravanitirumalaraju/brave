//
//  ChallengeListController.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ChallengeListController: UIAppController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource,UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet var challengesCollectionView: UICollectionView!
    @IBOutlet var challengeSelectBtn: BraveUIButton!
    @IBOutlet var coursesPickerVw: UIPickerView!
    @IBOutlet var titleLabel: BraveUILabel!
    
    var isSelected = Int()
    var challengeItems = [Int]()
    var allCoursesArray = [String]()
    var allCoursesId = [String]()
    var noDataString = String()
    var allChallengesArray = NSMutableArray()
    var courseIdSelected = String()
    var blurView = UIView()
    var popView = UIView()
    
    var isChallengedClicked = Bool()
    
    var selectedChallengesArray = NSArray()
    var selectedChallengeId = String()
    
    var isChallengeFinish = Bool()
    
    var purchaseArray = NSMutableArray()
    var CourseRow = Int()
    
    var purchaseBtn = UIButton()
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
//        noDataString = "Select the course"
        coursesPickerVw.isHidden = true
        
        UserDefaults.standard.set(true, forKey: "firstOpen")
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        isChallengeFinish = false
        if NetworkingManager.isConnectedToNetwork(){
            SVProgressHUD.show(withStatus: "Please wait..")
            self.blurView.isHidden = false
            popView.isHidden = true
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getCoursesURL, headers: headers).responseJSON { response in
                                print(response)
                switch response.result {
                case .success:
                    if let result = response.result.value {
                        self.allCoursesArray.removeAll()
                        self.purchaseArray.removeAllObjects()
                        let coursesResultArray = result as! NSArray
                        for value in coursesResultArray{
                            let valueDict = value as! NSDictionary
                            if (valueDict["makeThisCourse"] as! String) == "SHOW"{
                                self.purchaseArray.add(valueDict)
                                self.allCoursesArray.append(valueDict["courseName"] as! String)
                                self.allCoursesId.append(valueDict["_id"] as! String)
                            }
                        }
                        //                            let valueDict = value as! NSDictionary
                        //                            self.purchaseArray.add(valueDict)
                        //                            self.allCoursesArray.append(valueDict["courseName"] as! String)
                        //                            self.allCoursesId.append(valueDict["_id"] as! String)
                        //                        }
                        if let Id = UserDefaults.standard.value(forKey: "courseId") as? String{
                            //                            print(Id)
                            self.titleLabel.text = UserDefaults.standard.value(forKey: "courseName") as? String
                            //                            self.challengeSelectBtn.setTitle(UserDefaults.standard.value(forKey: "courseName") as? String, for: .normal)
                            self.courseIdSelected = Id
                            self.CourseRow = UserDefaults.standard.value(forKey: "row") as! Int
                                                        print(self.CourseRow)
                        }else{
                            self.titleLabel.text = self.allCoursesArray[0]
                            //                            self.challengeSelectBtn.setTitle(self.allCoursesArray[0], for: .normal)
                            self.courseIdSelected = self.allCoursesId[0]
                            self.CourseRow = 0
                        }
                        
                        self.challengesDisplay(courseId: self.courseIdSelected)
                    }
                case .failure(let error):
                    print(error)
                }
                //                debugPrint(response)
                //                SVProgressHUD.dismiss()
                //                self.blurView.isHidden = true
            }
        }else{
            SVProgressHUD.dismiss()
            print("no internet")
            alertBox(myMsg: "no internet connection")
        }
    }
    
    
    //MARK:- COLLECTIONVIEW DELEGATE & DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if challengeItems.count == 0{
            return 1
        }else{
            return challengeItems.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if challengeItems.count == 0{
            return CGSize(width: CGFloat(collectionView.frame.size.width), height: CGFloat(collectionView.frame.size.height))
        }else{
            return CGSize(width: CGFloat((collectionView.frame.size.width)/8), height: CGFloat(collectionView.frame.size.width)/8)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ChallengeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"challengeCollectionCell", for: indexPath as IndexPath) as! ChallengeCollectionCell
    //        cell.containerView.makeCircular()
            if challengeItems.count == 0{
                cell.noDataLabel.isHidden = false
                cell.itemTitle.text = ""
                cell.containerView.backgroundColor = AppColors.APP_COLOR_WHITE
                cell.noDataLabel.text = self.noDataString
                cell.noDataLabel.textColor = AppColors.APP_COLOR_PINK
                cell.noDataLabel.textAlignment = .center
            }else{
                cell.noDataLabel.isHidden = true
                cell.containerView.layer.cornerRadius = cell.frame.size.width / 2
                cell.containerView.clipsToBounds = true
                cell.itemTitle.text = String(challengeItems[indexPath.row])
                cell.itemTitle.textColor = AppColors.APP_COLOR_PINK
                cell.containerView.backgroundColor =  AppColors.APP_COLOR_WHITE
                
                if selectedChallengesArray.count == 0{
                    if allCoursesId[0] == courseIdSelected{
                        if indexPath.row == 0{
                            cell.itemTitle.textColor = AppColors.APP_COLOR_WHITE
                            cell.containerView.backgroundColor =  AppColors.APP_COLOR_PINK
                            let indDict = allChallengesArray[0] as! NSDictionary
                            selectedChallengesArray = [indDict["_id"] as! String]
                        }
                    }else{
                        if indexPath.row == 0{
//                            print(allChallengesArray)
                            cell.itemTitle.textColor = AppColors.APP_COLOR_WHITE
                            cell.containerView.backgroundColor =  AppColors.APP_COLOR_PINK
                            let indDict = allChallengesArray[0] as! NSDictionary
                            selectedChallengesArray = [indDict["_id"] as! String]
                        }
                    }
                }else{
                    for eachChallenge in selectedChallengesArray{
                        let indDict = allChallengesArray[indexPath.row] as! NSDictionary
                        if indDict["_id"] as! String == eachChallenge as! String{
                            cell.itemTitle.textColor = AppColors.APP_COLOR_WHITE
                            cell.containerView.backgroundColor =  AppColors.APP_COLOR_PINK
                            break
                        }
                    }
                }
            }
            cell.containerView.layer.borderColor = UIColor.gray.cgColor
            cell.containerView.layer.borderWidth = 1
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        SVProgressHUD.show(withStatus: "Please wait..")
//        self.blurView.isHidden = false
        if challengeItems.count != 0{
            isSelected = indexPath.row
//            if (UserDefaults.standard.value(forKey: "payUpdate") as? String == "YES") || (allCoursesId[0] == courseIdSelected && isSelected < 7){
            if (UserDefaults.standard.value(forKey: "payUpdate") as? String == "YES") || (UserDefaults.standard.value(forKey: "type") as? String == "FREE") || (UserDefaults.standard.value(forKey: "type") as? String == "TRIAL" && isSelected < 7){
                let dict = allChallengesArray[isSelected] as! NSDictionary
                for indChall in selectedChallengesArray{
                    if dict["_id"] as! String == indChall as! String{
                        selectedChallengeId = indChall as! String
                        isChallengeFinish = true
                        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//                        print(purchaseDict)
                        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(self.CourseRow, forKey: "row")
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async() { () -> Void in
                            self.performSegue(withIdentifier: "segueToChallengesController", sender: self.isSelected)
                        }
                        break
                    }
                }
                if isChallengeFinish == false{
                    alertBox(myMsg: "Complete the previous challenge")
                }
//                SVProgressHUD.dismiss()
//                self.blurView.isHidden = true
            }else{
//                SVProgressHUD.dismiss()
//                self.blurView.isHidden = true
//                self.courseAlertBox(myMsg: "This is a Paid Course ($9.99)")
                self.courseAlertBox(myMsg: "This is a Paid Course ($7.99)")
//                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
//                destVc.courseDetailsDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//                self.navigationController?.pushViewController(destVc, animated: true)
//                if indexPath.row >= 7{
//                    if (UserDefaults.standard.value(forKey: "payUpdate") as? String == "NO"){
//                        self.paymentNotDone()
//                    }
//                }
            }
        }
    }
    //MARK:- PICKER VIEW DELEGATE & DATASOURCE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allCoursesArray.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0{
             self.titleLabel.text = allCoursesArray[row]
//            challengeSelectBtn.setTitle(allCoursesArray[row] , for: .normal)
            pickerView.isHidden = true
            courseIdSelected = allCoursesId[row]
            CourseRow = row
            challengesDisplay(courseId: courseIdSelected)
        }else{
            print("courseFinish")
             self.titleLabel.text = allCoursesArray[row]
//            challengeSelectBtn.setTitle(allCoursesArray[row] , for: .normal)
            pickerView.isHidden = true
            courseIdSelected = allCoursesId[row]
            CourseRow = row
            challengesDisplay(courseId: courseIdSelected)
        }
        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//        print(purchaseDict)
        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.CourseRow, forKey: "row")
        UserDefaults.standard.synchronize()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: allCoursesArray[row], attributes: [NSAttributedStringKey.foregroundColor :
            AppColors.APP_COLOR_PINK])
        return attributedString
    }
    @IBAction func courseSelectedClicked(_ sender: Any) {
        self.coursesPickerVw.isHidden = false
        self.coursesPickerVw.delegate = self
        self.coursesPickerVw.dataSource = self
        if let row = UserDefaults.standard.value(forKey: "row") as? Int{
//            print(row)
            self.coursesPickerVw.selectRow(row, inComponent: 0, animated: true)
        }
    }
    //MARK:- CHALLENGE STATUS
    func challengesDisplay(courseId:String){
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        SVProgressHUD.show(withStatus: "Please wait..")
        self.blurView.isHidden = false
        Alamofire.request(getChallengeStatus + courseId, headers: headers).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSArray
                    self.selectedChallengesArray = JSON
                }
                self.getChallengeList(courseId: courseId)
            case .failure(let error):
                print(error)
            }
//            debugPrint(response)
        }
        //        getChallengeList(courseId: courseId)
    }
    //MARK:- CHALLENGE LIST
    func getChallengeList(courseId:String){
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        Alamofire.request(getChallengesListURL + courseId, headers: headers).responseJSON { response in
            print(response)
            switch(response.result) {
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSArray
                    self.challengeItems.removeAll()
                    self.allChallengesArray.removeAllObjects()
                    for (index, value) in JSON.enumerated(){
                        let dict = value as! NSDictionary
                        if let val = dict["message"] {
                            self.noDataString.removeAll()
                            self.noDataString = val as! String
                        }else{
                            self.allChallengesArray.add(value)
                            if (dict["challengeName"] as! String) != ""{
                                let challengeInt = Int(index)
                                self.challengeItems.append(challengeInt + 1)
                            }
                        }
                    }
                    self.challengesCollectionView.reloadData()
                    self.fetchPaymentDetails(courseId:courseId)
                }
            case .failure(_):
                print(response.result.error!)
                break
            }
//            debugPrint(response)
        }
        //        fetchPaymentDetails(courseId:courseId)
    }
    
    //MARK:- PAYMENT VIEW
    func fetchPaymentDetails(courseId:String){
        if NetworkingManager.isConnectedToNetwork(){
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getPaymentURL + courseId, headers: headers).responseJSON {
                response in
//                print(response)
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    if json["message"].string! == "Course Paid already"{
                        UserDefaults.standard.set("YES", forKey: "payUpdate")
                        UserDefaults.standard.synchronize()
                    }
//                    else if json["message"].string! == "Course Not Paid" {
//                        UserDefaults.standard.set("NO", forKey: "payUpdate")
//                        UserDefaults.standard.synchronize()
//                    }
                    else{
                        UserDefaults.standard.set("NO", forKey: "payUpdate")
                        UserDefaults.standard.synchronize()
                        let courseFinishDict = self.purchaseArray[self.CourseRow] as! NSDictionary
                        print(courseFinishDict)
                        let rowSelect = (UserDefaults.standard.value(forKey: "\(self.CourseRow)"))
                        if rowSelect == nil{
                            UserDefaults.standard.set(self.CourseRow, forKey: "\(self.CourseRow)")
                            UserDefaults.standard.synchronize()
                            if courseFinishDict["type"] as! String == "FREE"{
                                self.alertBox(myMsg: "This Course is FREE!")
                            }else if courseFinishDict["type"] as! String == "PAID"{
                                self.courseAlertBox(myMsg: "This is a Paid Course ($7.99)")
//                                self.courseAlertBox(myMsg: "This is a Paid Course ($9.99)")
                            }else if courseFinishDict["type"] as! String == "TRIAL"{
                                self.trailAlertBox(myMsg: "This Course has a FREE trial period of 7 Days")
                            }
                        }
                        if courseFinishDict["type"] as! String == "FREE"{
                            UserDefaults.standard.set("FREE", forKey: "type")
                        }else if courseFinishDict["type"] as! String == "PAID"{
                            UserDefaults.standard.set("PAID", forKey: "type")
                        }else if courseFinishDict["type"] as! String == "TRIAL"{
                            UserDefaults.standard.set("TRIAL", forKey: "type")
                        }
                        UserDefaults.standard.synchronize()
                    }
                case .failure(let error):
                    print(error)
                }
//                debugPrint(response)
            }
        }else{
            SVProgressHUD.dismiss()
            print("no internet")
            alertBox(myMsg: "no internet connection")
        }
    }
    //MARK:- TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:- PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextScene =  segue.destination as! ChallengesController
        nextScene.challengeDetailsDict = allChallengesArray[isSelected] as! NSDictionary
        nextScene.numberLabelString = String(isSelected + 1)
        if isSelected + 1 != allChallengesArray.count{
            let nextIdDict = allChallengesArray[isSelected + 1] as! NSDictionary
            let nextIdString = nextIdDict["_id"] as? String
            nextScene.nextChallengeIdString = nextIdString!
        }else{
            let nextIdDict = allChallengesArray[isSelected] as! NSDictionary
            let nextIdString = nextIdDict["_id"] as? String
            nextScene.nextChallengeIdString = nextIdString!
        }
        let unlockedIdString = selectedChallengesArray[selectedChallengesArray.count - 1]
        nextScene.unlockedChallengeIdString = unlockedIdString as! String
        
        if allCoursesId[0] == courseIdSelected{
            if isSelected == 0{
                addingFirstChallenge()
            }
        }else{
            if isSelected == 0{
                addingFirstChallenge()
            }
        }
    }
    func addingFirstChallenge(){
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let userId = UserDefaults.standard.value(forKey: "userId") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        var statusParams = [String:Any]()
        let statusDict = allChallengesArray[isSelected] as! NSDictionary
        statusParams = ["userId":userId!,"courseId":statusDict["courseId"] as? String as Any,"challengeId":statusDict["_id"] as! String]
        Alamofire.request(challengeStatusURL, method: .post, parameters: statusParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//            print(response)
            switch response.result {
            case .success:
                break
            case .failure(let error):
                print(error)
            }
//            debugPrint(response)
        }
    }
   
    //MARK:- PROFILE BUTTON TAPPED
    @IBAction func profileBtnTapped(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "profileController") as! ProfileController
        self.navigationController?.pushViewController(destVc, animated: true)
    }

    //MARK:- ALERTBOX
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
            let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//            print(purchaseDict)
            UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(self.CourseRow, forKey: "row")
            UserDefaults.standard.synchronize()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- ALERTS FOR COURSES
    func courseAlertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "PAY", style: .default, handler: { UIAlertAction in
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
            destVc.courseDetailsDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//            let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//            print(purchaseDict)
//            UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
//            UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
//            UserDefaults.standard.set(self.CourseRow, forKey: "row")
//            UserDefaults.standard.synchronize()
            self.navigationController?.pushViewController(destVc, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//        print(purchaseDict)
        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.CourseRow, forKey: "row")
        UserDefaults.standard.synchronize()
        
//        print(allCoursesArray)
//        print(allCoursesId)
//        print(self.CourseRow)
        
    }
    
    func trailAlertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "START", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//        print(purchaseDict)
        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.CourseRow, forKey: "row")
        UserDefaults.standard.synchronize()
    }
    
    
    //MARK:- PAYMENT APPLIED
    func paymentNotDone(){
        blurView.isHidden = false
        popView.isHidden = false
        
        popView.frame = CGRect(x: 0, y: 0, width: 300, height: 70)
        popView.center.x = blurView.center.x
        popView.center.y = blurView.center.y
        popView.backgroundColor = AppColors.APP_COLOR_WHITE
        self.blurView.addSubview(popView)
        
        let cancelButton = UIButton.init(frame: CGRect(x: popView.frame.size.width-40, y: 10, width: 30, height: 30))
        cancelButton.setImage(UIImage(named : "cross_blue"), for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        popView.addSubview(cancelButton)
        
        purchaseBtn.frame = CGRect(x: 50, y: popView.frame.size.height-50, width: 200, height: 40)
        purchaseBtn.backgroundColor = AppColors.APP_COLOR_PINK
        purchaseBtn.setTitle("Purchase", for: .normal)
//        purchaseBtn.addTarget(self, action: #selector(purchaseCourseClicked), for: UIControlEvents.touchUpInside)
        popView.addSubview(purchaseBtn)
        
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        blurView.isHidden = true
        popView.isHidden = true
    }
    
    //MARK:- PURCHASE COURSE CICKED
//    @objc func purchaseCourseClicked() {
//        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
//        destVc.courseDetailsDict = purchaseArray[CourseRow] as! NSDictionary
//        self.navigationController?.pushViewController(destVc, animated: true)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
