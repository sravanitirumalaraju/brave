//
//  ChallengesController.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import SVProgressHUD
import Alamofire

class ChallengesController: UIAppController,UIScrollViewDelegate {

    @IBOutlet var challengeNameLbl: BraveUILabel!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var playImageView: UIImageView!
    @IBOutlet var minTimeDurationLbl: UILabel!
    @IBOutlet var maxTimeDurationLbl: UILabel!
    @IBOutlet var sliderVolume: UISlider!
    @IBOutlet var challengeImage: UIImageView!
    @IBOutlet var name_challengeLabel: BraveUILabel!
    @IBOutlet var number_Label: BraveUILabel!
    @IBOutlet var description_ScrollView: UIScrollView!
    @IBOutlet weak var markAsCompletedLbl: BraveUILabel!
    
    var audioPlayer:AVAudioPlayer!
    var isAudioPlayed = Bool()
    var challengeDetailsDict = NSDictionary()
    var blurView = UIView()
    var numberLabelString = String()
    
    var descriptionLbl = UILabel()
    var readMoreBtn = UIButton()
    var LabelHeight = UILabel()
    
    var nextChallengeIdString = String()
    var unlockedChallengeIdString = String()
    
    var linkButton = UIButton()
    let linkLbl = UILabel()
    var challengeAlreadyCompleted: Bool = false
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        linkLbl.isHidden = true
        linkButton.isHidden = true
//        print(challengeDetailsDict)
        descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: 65)
        descriptionLbl.numberOfLines = 0
        descriptionLbl.center.x = self.view.center.x
        descriptionLbl.backgroundColor = UIColor.clear
        descriptionLbl.textAlignment = .center
        descriptionLbl.text = challengeDetailsDict["description"] as? String
        description_ScrollView.addSubview(descriptionLbl)

        LabelHeight.frame = CGRect(x: 0, y: 10, width: 300, height: LabelHeight.frame.height)
        LabelHeight.text = challengeDetailsDict["description"] as? String
        LabelHeight.numberOfLines = 0
        LabelHeight.sizeToFit()
        LabelHeight.center.x = self.view.center.x
        LabelHeight.textAlignment = .center
        
        readMoreBtn.isHidden = true
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        SVProgressHUD.show(withStatus: "Please wait..")
        self.blurView.isHidden = false
//        print(challengeDetailsDict["challengeFilePath"] as! String)
//        print(challengeDetailsDict)
        if let audioURLString = challengeDetailsDict["challengeFilePath"] as? String{
            let audioFilePath = "\(challengePath)\(String(describing: audioURLString))"
            let url = NSURL(string: audioFilePath)
            print(url!)
            downloadFileFromURL(url: url!)
        }else{
            SVProgressHUD.dismiss()
            self.blurView.isHidden = true
            alertBox(myMsg: "No audio file found")
        }
        
        challengeNameLbl.text = challengeDetailsDict["challengeName"] as? String
        name_challengeLabel.text = challengeDetailsDict["challengeName"] as? String
        number_Label.text = numberLabelString
//        print(challengeDetailsDict["challengeImage"] as! String)
        if (challengeDetailsDict["challengeImage"] as! String) != "https://d3btcp4i8nc67d.cloudfront.net/challengesimages/course-BRAVE/undefined"{
            if let imageURLString = challengeDetailsDict["challengeImage"] as? String {
                let url = URL(string: imageURLString)
                let data = try? Data(contentsOf: url!)
                self.challengeImage.image = UIImage(data: data!)
            }
        }

//        print(LabelHeight.frame.size.height)
//        print(description_ScrollView.frame.size.height)
        
        if LabelHeight.frame.size.height > description_ScrollView.frame.size.height{
            descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: description_ScrollView.frame.size.height - 20)
            
            readMoreBtn.isHidden = false
            readMoreBtn.frame = CGRect(x: 0, y: descriptionLbl.frame.origin.y + descriptionLbl.frame.size.height, width: 100, height: 20)
            readMoreBtn.setTitle("Read more", for: .normal)
            readMoreBtn.center.x = self.view.center.x
            readMoreBtn.backgroundColor = UIColor.clear
            readMoreBtn.setTitleColor(AppColors.APP_COLOR_PINK, for: .normal)
            readMoreBtn.addTarget(self, action: #selector(readMore_tapped(_:)), for: .touchUpInside)
            description_ScrollView.addSubview(readMoreBtn)
        }else{
            descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: LabelHeight.frame.height)
            if let linkURLString = challengeDetailsDict["challengeUrl"] as? String {
                linkLbl.isHidden = false
                linkButton.isHidden = false
                linkLbl.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 5, width: 300, height: linkLbl.frame.height)
                linkLbl.text = linkURLString
                linkLbl.numberOfLines = 0
                linkLbl.textColor = UIColor(rgb: 0xFF4081)
                linkLbl.textAlignment = .center
                linkLbl.sizeToFit()
                linkLbl.center.x = self.view.center.x
                description_ScrollView.addSubview(linkLbl)
                
                linkButton.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 2, width: 300, height: linkLbl.frame.size.height)
                linkButton.center.x = self.view.center.x
                linkButton.addTarget(self, action: #selector(linkClicked), for: .touchUpInside)
                description_ScrollView.addSubview(linkButton)
                
            }
        }
        descriptionLbl.center.x = self.view.center.x
        descriptionLbl.textAlignment = .center
//        if LabelHeight.frame.size.height > 65{
//            readMoreBtn.isHidden = false
//        }else{
//            descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: LabelHeight.frame.height)
//        }
        var contentRect = CGRect.zero
        for view in description_ScrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        description_ScrollView.contentSize.height = contentRect.size.height
    }
    
    func downloadFileFromURL(url:NSURL){
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            do {
                if URL != nil{
                    try self?.audioPlayer = AVAudioPlayer(contentsOf: URL!)
                    self?.audioPlayer.delegate = self as? AVAudioPlayerDelegate
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self?.blurView.isHidden = true
                        self?.maxTimeDurationLbl.text = self?.timeString(time: TimeInterval((self?.audioPlayer.duration)!))
                    }
                }else{
                    SVProgressHUD.dismiss()
                    self?.blurView.isHidden = true
                    self?.alertBox(myMsg: "No audio found")
                }
            }catch {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self?.blurView.isHidden = true
                    print(error)
                    self?.alertBox(myMsg: "No Audio file is found!")
                }
            }
        })
        downloadTask.resume()
    }
    
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: doSomething))
        self.present(alert, animated: true, completion: nil)
    }
    func doSomething(action: UIAlertAction) {
        self.blurView.isHidden = true
    }
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(_ animated: Bool) {
        if maxTimeDurationLbl.text! != "00:00"{
            audioPlayer.stop()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        checkIfChallengeCompleted()
    }
    //MARK:- BACK BUTTON TAPPED
    @IBAction func backButtonTapped(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    //MARK:- MARK COMPLETED TAPPED
    @IBAction func markAsCompletedTapped(_ sender: UIButton) {
        if challengeAlreadyCompleted {
            alertBox(myMsg: "Already completed the challenge")
            return
        }
        if nextChallengeIdString != ""{
            if NetworkingManager.isConnectedToNetwork(){
                SVProgressHUD.show(withStatus: "Please wait..")
                self.blurView.isHidden = false
                //retrieve the token and add it to header
                let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
                let tokenString = "Bearer " + retrievedToken!
                let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
                let statusParams = ["courseId":challengeDetailsDict["courseId"] as? String,"challengeId":nextChallengeIdString] as! [String : String]
                Alamofire.request(challengeStatusURL, method: .post, parameters: statusParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    SVProgressHUD.dismiss()
                    self.blurView.isHidden = true
                    switch response.result {
                    case .success://gautham
//                        if UserDefaults.standard.bool(forKey: "autoRemindNotifications") { //sravani
//                            AppDelegate.queueNotifications()
//                        }
                        self.performSegue(withIdentifier: "segueToShareProgressController", sender: self)
//                    case .success:
//                        self.performSegue(withIdentifier: "segueToShareProgressController", sender: self)
                    case .failure(let error):
                        print(error)
                    }
                    debugPrint(response)
                }
            }else{
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                print("no internet")
                alertBox(myMsg: "no internet connection")
            }
        }
    }

    @IBAction func playAudio(_ sender: Any) {
        if maxTimeDurationLbl.text! == "00:00"{
            alertBox(myMsg: "cannot play this audio")
        }else{
            if isAudioPlayed == false{
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                     try AVAudioSession.sharedInstance().setActive(true)
                }
                catch {
                    // report for an error
                }
                
                audioPlayer.play()
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
                progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: false)
                //            maxTimeDurationLbl.text = timeString(time: TimeInterval(audioPlayer.duration))
                minTimeDurationLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
                isAudioPlayed = true
                playImageView.image = UIImage(named: "pause")
            }else{
                audioPlayer.stop()
                isAudioPlayed = false
                playImageView.image = UIImage(named: "play")
            }
        }
    }
   
    @IBAction func backForward(_ sender: Any) {
        if maxTimeDurationLbl.text! == "00:00"{
            alertBox(myMsg: "cannot play this audio")
        }else{
            var time: TimeInterval = audioPlayer.currentTime
            time -= 10.0 // Go back by 5 seconds
            if playImageView.image == UIImage(named: "pause"){
                audioPlayer.currentTime = time
                minTimeDurationLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
            }else{
                print("audio paused")
            }
        }
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        if maxTimeDurationLbl.text! == "00:00"{
            alertBox(myMsg: "cannot change the volume")
        }else{
            audioPlayer.volume = sliderVolume.value
        }
    }
    @IBAction func fastForward(_ sender: Any) {
        if maxTimeDurationLbl.text! == "00:00"{
            alertBox(myMsg: "cannot play this audio")
        }else{
            var time: TimeInterval = audioPlayer.currentTime
            time += 10.0 // Go forward by 5 seconds
            if audioPlayer.currentTime == audioPlayer.duration{
                audioPlayer.play()
                playImageView.image = UIImage(named: "pause")
                //            audioPlayer.stop()
            }else{
                if playImageView.image == UIImage(named: "pause"){
                    audioPlayer.currentTime = time
                    minTimeDurationLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
                }
            }
        }
    }
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
         return String(format: "%02i:%02i", minutes, seconds)
    }
    @objc func updateAudioProgressView(){
        if audioPlayer.isPlaying{
            // Update progress
            progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: true)
            minTimeDurationLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
            if minTimeDurationLbl.text == timeString(time: TimeInterval(audioPlayer.duration)){
                playImageView.image = UIImage(named: "play")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func linkClicked(_ sender: Any) {
        let destVC: WebViewController = self.storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        destVC.receiveURL = challengeDetailsDict["challengeUrl"] as! String
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    @objc func readMore_tapped(_ sender: Any) {
        descriptionLbl.sizeToFit()
        readMoreBtn.isHidden = true
        
//        print(descriptionLbl.frame.size.height)
//        print(descriptionLbl.frame.origin.y)
        
        linkLbl.isHidden = false
        linkButton.isHidden = false
        linkLbl.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 5, width: 300, height: linkLbl.frame.height)
        linkLbl.text = challengeDetailsDict["challengeUrl"] as? String
        linkLbl.numberOfLines = 0
        linkLbl.textColor = UIColor(rgb: 0xFF4081)
        linkLbl.textAlignment = .center
        linkLbl.sizeToFit()
        linkLbl.center.x = self.view.center.x
        description_ScrollView.addSubview(linkLbl)
        
        linkButton.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 2, width: 300, height: linkLbl.frame.size.height)
        linkButton.center.x = self.view.center.x
        linkButton.addTarget(self, action: #selector(linkClicked), for: .touchUpInside)
        description_ScrollView.addSubview(linkButton)
        
//        print(description_ScrollView.frame.size.height)
        var contentRect = CGRect.zero
        for view in description_ScrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        description_ScrollView.contentSize.height = contentRect.size.height
    }
    func checkIfChallengeCompleted() {
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        let courseId = challengeDetailsDict["courseId"] as! String
        let challengeId = challengeDetailsDict["_id"] as! String
        Alamofire.request(getChallengeStatus + courseId, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSArray
                    if JSON.contains(challengeId) && (JSON.lastObject as! String != challengeId) {
                        self.challengeAlreadyCompleted = true
                        self.markAsCompletedLbl.text = "Already Completed"
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
