//
//  PurchaseViewController.swift
//  Brave
//
//  Created by apple on 27/11/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import StoreKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class PurchaseViewController: UIViewController,SKProductsRequestDelegate,SKPaymentTransactionObserver {

    @IBOutlet var courseAmount: UILabel!
    @IBOutlet var courseName: UILabel!
    
    var courseDetailsDict = NSDictionary()
    var productsArray: Array<SKProduct?> = []
    var transactionInProgress = false
    var amountTxt = String()
    var blurView = UIView()
    
    var courseSelect = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
       
        courseSelect = UserDefaults.standard.value(forKey: "row") as! Int
        courseName.text = courseDetailsDict["courseName"] as? String
        amountTxt = "$"
        courseAmount.text =  amountTxt.appending(courseDetailsDict["courseAmount"] as! String)
        print(courseDetailsDict)
        requestProductInfo()
        SKPaymentQueue.default().add(self)
    }
    func requestProductInfo() {
        SVProgressHUD.show(withStatus: "Please wait..")
        blurView.isHidden = false
        if SKPaymentQueue.canMakePayments() {
            let productRequest = SKProductsRequest(productIdentifiers: Set(["com.braveapp.foundation","com.braveapp.braverunning","com.braveapp.foundations2"]))
            print(productRequest)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        SVProgressHUD.dismiss()
        blurView.isHidden = true
        print(response)
        if response.products.count != 0 {
            for product in response.products {
                productsArray.append(product)
            }
        }
        else {
            alertBox(myMsg: "There are no products.")
        }
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        SVProgressHUD.show(withStatus: "Please wait..")
        self.blurView.isHidden = false
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                purchaseSuccess()
            case SKPaymentTransactionState.failed:
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                alertBox(myMsg: "Transaction Failed")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
            case SKPaymentTransactionState.restored:
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                SKPaymentQueue.default().finishTransaction(transaction)
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    func purchaseSuccess() {
        if NetworkingManager.isConnectedToNetwork() {
            //retrieve the token and add it to header
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            let updatePayParams = ["courseId":courseDetailsDict["_id"] as! String,"paidAmount":courseDetailsDict["courseAmount"] as! String]
            let paymentParams = ["payment":updatePayParams]
            print(paymentParams)
            Alamofire.request(updatePaymentURL, method: .post, parameters: paymentParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    self.alertBox(myMsg: json["message"].string!)
                case .failure(let error):
                    print(error)
                }
                debugPrint(response)
            }
        }else{
            SVProgressHUD.dismiss()
            self.blurView.isHidden = true
            alertBox(myMsg: "no internet connection")
        }
    }
    //MARK:- ALERTBOX
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func buyBtnTapped(_ sender: Any) {
        print(productsArray as NSArray)
        if courseSelect == 0{
            let payment = SKPayment(product: self.productsArray[0]!)
            SKPaymentQueue.default().add(payment)
        }else if courseSelect == 1{
            let payment = SKPayment(product: self.productsArray[1]!)
            SKPaymentQueue.default().add(payment)
        }else if courseSelect == 2{
            let payment = SKPayment(product: self.productsArray[2]!)
            SKPaymentQueue.default().add(payment)
        }
//        let payment = SKPayment(product: self.productsArray[1]!)
//        SKPaymentQueue.default().add(payment)
        self.transactionInProgress = true
    }
    // MARK: - RESTORE NON-CONSUMABLE PURCHASE BUTTON

    @IBAction func restoreBtnTapped(_ sender: Any) {
        if (SKPaymentQueue.canMakePayments()) {
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().restoreCompletedTransactions()
        }
    }

    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        alertBox(myMsg: "You've successfully restored your purchase!")
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        alertBox(myMsg: "restore transaction failed")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backClicked(_ sender: Any) {
        performSegueToReturnBack()
    }
}
