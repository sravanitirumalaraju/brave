//
//  UIAppController.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit

class UIAppController: UIViewController {

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        addInitialUIChanges()
    }
    func addInitialUIChanges()
    {
        self.automaticallyAdjustsScrollViewInsets =  false
        self.navigationController?.navigationBar.isHidden =  true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.resignFirstResponder()
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}
