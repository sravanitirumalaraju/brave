//
//  SignUpViewController.swift
//  Brave
//
//  Created by apple on 30/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SignUpViewController: UIViewController,UITextFieldDelegate {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet var userNameTxtFld: BraveUITextField!
    @IBOutlet var emailAddressTxtFld: BraveUITextField!
    @IBOutlet var passwordTxtFld: BraveUITextField!
    @IBOutlet var mobileTxtFld: BraveUITextField!
    @IBOutlet var loginBtn: UIButton!
    
    var blurView = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        mobileTxtFld.delegate = self
        let text = "Have an account? LOGIN instead"
        let linkTextWithColor = "LOGIN"
        let remainTextRange = (text as NSString).range(of: text)
        let range = (text as NSString).range(of: linkTextWithColor)
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: AppColors.APP_COLOR_WHITE, range: remainTextRange)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: AppColors.APP_COLOR_PINK , range: range)
        loginBtn.setAttributedTitle(attributedString, for: .normal)
        
        passwordTxtFld.isSecureTextEntry = true
        passwordTxtFld.spellCheckingType = .no
        passwordTxtFld.autocorrectionType = .no
        // Do any additional setup after loading the view.
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.view.frame.size.height == 568{
            if textField.tag == 3{
                self.view.frame.origin.y -= 50
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.size.height == 568{
            if textField.tag == 3{
                self.view.frame.origin.y += 50
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func signInTapped(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if userNameTxtFld.text! == "" && emailAddressTxtFld.text! == "" && mobileTxtFld.text! == "" && passwordTxtFld.text! == ""{
                alertBox(myMsg: "please enter username & emailId & mobile number & password")
            }
            else if userNameTxtFld.text! == "" {
                alertBox(myMsg: "please enter userName")
            }else if emailAddressTxtFld.text! == ""{
                alertBox(myMsg: "please enter emailId")
            }else if mobileTxtFld.text! == ""{
                alertBox(myMsg: "please enter mobile number")
            }else if passwordTxtFld.text! == ""{
                alertBox(myMsg: "please enter password")
            }else{
                let isEmailAddressValid = isValidEmailAddress(emailAddressString: emailAddressTxtFld.text!)
                if isEmailAddressValid{
                    SVProgressHUD.show(withStatus: "Please wait..")
                    self.blurView.isHidden = false
                    let signInParams = ["userName":"\(userNameTxtFld.text!)","emailId":"\(emailAddressTxtFld.text!)","mobileNumber":"\(mobileTxtFld.text!)","password":"\(passwordTxtFld.text!)"]
                    print(passwordTxtFld.text!)
                    print(signInParams)
                    let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                    Alamofire.request(signInURL, method: HTTPMethod.post, parameters: signInParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                        SVProgressHUD.dismiss()
                        self.blurView.isHidden = true
                        print(response)
                        switch(response.result) {
                        case .success(let value):
                            let json = JSON(value)
                            print(json)
                            if response.response?.statusCode == 200{
                                print("sucess")
                                if let sucessValue = json["success"].string{
                                    self.alertBox(myMsg: sucessValue)
                                }else{
                                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "challengeListController") as! ChallengeListController
                                    self.navigationController?.pushViewController(destVc, animated: true)
                                    UserDefaults.standard.set(self.emailAddressTxtFld.text!, forKey: "emailId")
                                    let stringValue = json["token"].string!
                                    print("the value is : \(stringValue)")
                                    UserDefaults.standard.setValue(stringValue, forKey: "token")
                                    UserDefaults.standard.set(json["userId"].string!, forKey: "userId")
                                    UserDefaults.standard.synchronize()
                                    self.appRegisteration()
                                }
                            }
                        case .failure(_):
                            SVProgressHUD.dismiss()
                            self.blurView.isHidden = true
                            print(response.result.error!)
                            print("error")
                            break
                        }
                        debugPrint(response)
                    }
                }else{
                    alertBox(myMsg: "Please enter valid email id")
                }
            }
        }else{
             SVProgressHUD.dismiss()
            blurView.isHidden = true
            alertBox(myMsg: "no internet connection")
        }
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0{
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
//    func callToServerAPI(_ serviceURL:String, parameters: AnyObject){
//        if NetworkingManager.isConnectedToNetwork(){
////            SVProgressHUD.show(withStatus: ProgressHud.PLEASE_WAIT)
//            NetworkingManager.postDataToServer(parameters, serviceURL: serviceURL, didFinish: { json in
////                SVProgressHUD.dismiss()
//                print(json)
//            }, didFail: { error in
//                print("error")
//            })
//        }else {
////            let alert = AlertView.alertWithTitle(title: Messages.NO_INTERNET_TITLE, message: Messages.NO_INTERNET_MESSAGE)
////            alert.addButtons(buttonArray: ["Okay", "Settings"])
//        }
//
//     }
    @IBAction func loginInsteadTapped(_ sender: Any) {
        let destVc = storyboard?.instantiateViewController(withIdentifier: "loginController") as! LoginController
        navigationController?.pushViewController(destVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- APP REGISTERATION
    @objc func appRegisteration(){
        if Platform.isSimulator == false{
            if let FCMToken: String = UserDefaults.standard.value(forKey: "FCM_Token") as? String{
                if FCMToken != "" {
//            if UserDefaults.standard.value(forKey: "FCM_Token") as! String != ""{
                let registerationId = UserDefaults.standard.value(forKey: "FCM_Token") as! String
                let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let registerParams = ["registrationId":registerationId]
                Alamofire.request(appRegisterationURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                    switch (response.result){
                    case .success:
                        print(response.result.value!)
                    case .failure:
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                }
            }
            }
        }
    }
}
