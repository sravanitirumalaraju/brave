//
//  ShareProgressController.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit

class ShareProgressController: UIAppController {
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK:- BACK BUTTON TAPPED
    @IBAction func backButtonTapped(_ sender: UIButton) {
//        performSegueToReturnBack()
        let destVc = storyboard?.instantiateViewController(withIdentifier: "challengeListController") as! ChallengeListController
        navigationController?.pushViewController(destVc, animated: true)
    }

    @IBAction func facebookBtnTapped(_ sender: Any) {
        UIApplication.tryURL(urls: ["https://www.facebook.com/groups/1934941283390233"])
    }
    
    @IBAction func googleBtnTapped(_ sender: Any) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                if #available(iOS 10.0, *) {
                    application.open(URL(string: url)!)
                } else {
                    // Fallback on earlier versions
                }
//                application.openURL(URL(string: url)!)
                return
            }
        }
    }
}
