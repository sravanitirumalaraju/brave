//
//  LoginController.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class LoginController: UIAppController, UITextFieldDelegate {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet var emailTextField:  BraveUITextField!
    @IBOutlet var passwordTextField: BraveUITextField!
    @IBOutlet var loginButton: BraveUIButton!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var signUpBtn: UIButton!
    
    var blurView = UIView()
    var popUpView = UIView()
    let enterEmailTxtFld = BraveUITextField()
    let countrySelectBtn = UIButton()
    var countryCodeVw = UIView()
    let countryCodeLbl = UILabel()

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        let text = "Don't have an account? SIGN UP instead"
        let linkTextWithColor = "SIGN UP"
        let remainTextRange = (text as NSString).range(of: text)
        let range = (text as NSString).range(of: linkTextWithColor)
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: AppColors.APP_COLOR_WHITE, range: remainTextRange)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: AppColors.APP_COLOR_PINK , range: range)
        signUpBtn.setAttributedTitle(attributedString, for: .normal)
        passwordTextField.isSecureTextEntry = true
        passwordTextField.spellCheckingType = .no
        passwordTextField.autocorrectionType = .no
    }
    //MARK:- TOUCH BEGAN
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //MARK:- TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    //MARK:- SUBMIT BUTTON TAPPED
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if NetworkingManager.isConnectedToNetwork(){
            print(emailTextField.text!)
            if emailTextField.text! == "" && passwordTextField.text! == ""{
                alertBox(myMsg: "please enter emailId & password")
            }
            else if emailTextField.text! == "" {
                alertBox(myMsg: "please enter emailId")
            }
            else if passwordTextField.text! == ""{
                alertBox(myMsg: "please enter password")
            }
            else{
                let isEmailAddressValid = isValidEmailAddress(emailAddressString: emailTextField.text!)
                if isEmailAddressValid{
                    SVProgressHUD.show(withStatus: "Please wait..")
                    self.blurView.isHidden = false
                    let loginParams = ["emailId":emailTextField.text!,"password":passwordTextField.text!]
                    let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                    Alamofire.request(loginURL, method: .post, parameters: loginParams, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                        print(response)
                        SVProgressHUD.dismiss()
                        self.blurView.isHidden = true
                        switch(response.result) {
                        case .success(let value):
                            let json = JSON(value)
                            print(json)
                            if response.response?.statusCode == 200{
                                print("success")
                                self.performSegue(withIdentifier: "segueToChallengeListController", sender: self)
                                UserDefaults.standard.set(self.emailTextField.text!, forKey: "emailId")
                                UserDefaults.standard.set(json["userId"].string!, forKey: "userId")
                                UserDefaults.standard.synchronize()
                                let stringValue = json["token"].string!
                                print("the value is : \(stringValue)")
                                UserDefaults.standard.setValue(stringValue, forKey: "token")
                                UserDefaults.standard.synchronize()
//                                UserDefaults.standard.set(true, forKey: "autoRemindNotifications")//gautham
//                                UserDefaults.standard.synchronize()
                                self.appRegisteration()
                            }
                            else{
                                if let sucessValue = json["message"].string{
                                    if sucessValue == "NO_EMAIL"{
                                        self.alertBox(myMsg: "You are not Registered user with this email ")
                                    }else{
                                        self.alertBox(myMsg: "You entered wrong password")
                                    }
                                }
                            }
                        case .failure(_):
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }else{
                    alertBox(myMsg: "Please enter valid email id")
                }
            }
        }else{
            SVProgressHUD.dismiss()
            print("no network")
            alertBox(myMsg: "no internet connection")
        }
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0{
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        blurView.isHidden = false
        popUpView.isHidden = false
        enterEmailTxtFld.text = ""

        popUpView.frame = CGRect(x: 0, y: 0, width: 300, height: 160)
        popUpView.center = self.blurView.center
        popUpView.backgroundColor = AppColors.APP_COLOR_WHITE
        popUpView.layer.cornerRadius = 10
        popUpView.clipsToBounds = true
        blurView.addSubview(popUpView)
        
        let cancelBtn = UIButton.init(frame: CGRect(x: popUpView.frame.size.width-40, y: 10, width: 30, height: 30))
        cancelBtn.setImage(UIImage(named:"cross_blue"), for: .normal)
        cancelBtn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        popUpView.addSubview(cancelBtn)
        
        enterEmailTxtFld.frame = CGRect(x: 25, y: 45, width: 250, height: 40)
        enterEmailTxtFld.placeholder = "Enter Email"
        enterEmailTxtFld.textAlignment = .center
        enterEmailTxtFld.keyboardType = .emailAddress
        enterEmailTxtFld.autocapitalizationType = .none
        enterEmailTxtFld.autocorrectionType = .no
        enterEmailTxtFld.delegate = self
        popUpView.addSubview(enterEmailTxtFld)
        
        let submitBtn = UIButton.init(frame: CGRect(x: 50, y: enterEmailTxtFld.frame.size.height + enterEmailTxtFld.frame.origin.y + 20, width: 200, height: 40))
        submitBtn.setTitle("Submit", for: .normal)
        submitBtn.layer.cornerRadius = submitBtn.frame.size.height/2
        submitBtn.backgroundColor = AppColors.APP_COLOR_PINK
        submitBtn.addTarget(self, action: #selector(submitClicked), for: .touchUpInside)
        popUpView.addSubview(submitBtn)
    }
    @objc func submitClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            SVProgressHUD.show()
            let forgotParams = ["email":enterEmailTxtFld.text!]
            let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
            Alamofire.request(forgotURL, method: .put, parameters: forgotParams, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                print(response)
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                switch(response.result) {
                case .success(let value):
                    let json = JSON(value)
                   if response.response?.statusCode == 200{
                        self.alertBox(myMsg: "Password has been sent to your email please check your email")
                        self.popUpView.isHidden = true
                        self.blurView.isHidden = true
                   }else{
                        self.alertBox(myMsg: json["message"].string!)
                   }
                case .failure :
                    print(response.result.error!)
                    break
                }
            }
        }else{
            alertBox(myMsg: "No internet connection")
        }
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        popUpView.isHidden = true
        blurView.isHidden = true
    }
    @IBAction func facebookLoginClicked(_ sender: Any) {
        
    }
    @IBAction func googleLoginClicked(_ sender: Any) {
    }
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- APP REGISTERATION
    @objc func appRegisteration(){
        if Platform.isSimulator == false{
            
            if let FCMToken: String = UserDefaults.standard.value(forKey: "FCM_Token") as? String{
                if FCMToken != "" {
                    let registerationId = UserDefaults.standard.value(forKey: "FCM_Token") as! String
                    let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
                    let tokenString = "Bearer " + retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    let registerParams = ["registrationId":registerationId]
                    Alamofire.request(appRegisterationURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result) {
                        case .success:
                            print(response.result.value!)
                        case .failure:
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }
            }
//            if UserDefaults.standard.value(forKey: "FCM_Token") as! String != ""{
//                let registerationId = UserDefaults.standard.value(forKey: "FCM_Token") as! String
//                let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
//                let tokenString = "Bearer " + retrievedToken!
//                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
//                let registerParams = ["registrationId":registerationId]
//                Alamofire.request(appRegisterationURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
//                    switch (response.result){
//                    case .success:
//                        print(response.result.value!)
//                    case .failure:
//                        print(response.result.error!)
//                        break
//                    }
//                    debugPrint(response)
//                }
//            }
        }
    }
}

