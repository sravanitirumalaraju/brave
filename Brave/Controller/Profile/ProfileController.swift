//
//  ProfileController.swift
//  Brave
//
//  Created by apple on 10/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ProfileController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var userProfileButton: UIButton!
    @IBOutlet var userProfileImage: UIImageView!
    @IBOutlet var mobileTxtFld: BraveUITextField!
    @IBOutlet var passwordTxtFld: BraveUITextField!
    @IBOutlet var emailTxtFld: BraveUITextField!
    @IBOutlet var saveBtn: BraveUIButton!
    @IBOutlet var editBtn: UIButton!
    @IBOutlet var userNameTxtFld: UITextField!
    
    var userId = String()
    var imagePicker = UIImagePickerController()
    var blurView = UIView()
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        passwordTxtFld.delegate = self
        mobileTxtFld.delegate = self
        userNameTxtFld.delegate = self
        imagePicker.delegate = self
        userProfileImage.layer.cornerRadius = userProfileImage.frame.size.width / 2
        userProfileImage.layer.masksToBounds = true
        let userEmailId = UserDefaults.standard.value(forKey: "emailId")
        fetchUserDetails(emailId: userEmailId as! String)
    }
    func fetchUserDetails(emailId:String){
        if NetworkingManager.isConnectedToNetwork(){
            SVProgressHUD.show(withStatus: "Please wait..")
            self.blurView.isHidden = false
            //retrieve the token and add it to header
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let userHeaders: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getUserDetailsURL + emailId, headers: userHeaders).responseString { response in
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                print(response)
                switch(response.result) {
                case .success:
                    let queryData = self.convertToDictionary(text: response.result.value!)
//                    print(queryData)
                    let userResult = queryData!["result"] as! NSDictionary
                    self.emailTxtFld.text = userResult["emailId"] as? String
                    self.passwordTxtFld.text = userResult["password"] as? String
                    self.mobileTxtFld.text = userResult["mobileNumber"] as? String
                    self.userId = userResult["_id"] as! String
                    self.userNameTxtFld.text = userResult["userName"] as? String
                    if let userImageString = userResult["userImage"] as? String{
                        let url = URL(string: userImageString)
                        let data = try? Data(contentsOf: url!)
                        self.userProfileImage.image = UIImage(data: data!)
                    }
                case .failure(_):
                    SVProgressHUD.dismiss()
                    self.blurView.isHidden = true
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            SVProgressHUD.dismiss()
            print("no internet")
            alertBox(myMsg: "no internet connection")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.view.frame.size.height == 568{
            if textField.tag == 1 || textField.tag == 2{
                self.view.frame.origin.y -= 75
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.size.height == 568{
            if textField.tag == 1 || textField.tag == 2{
                self.view.frame.origin.y += 75
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func saveBtnTapped(_ sender: Any) {
        userEditedDetails(id: userId)
    }
    func userEditedDetails(id:String){
        if NetworkingManager.isConnectedToNetwork(){
            SVProgressHUD.show(withStatus: "Please wait..")
            self.blurView.isHidden = false
            //retrieve the token and add it to header
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let userHeaders: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            let updatedUserParams = ["userName":userNameTxtFld.text!,"emailId":emailTxtFld.text!,"mobileNumber":mobileTxtFld.text!,"password":passwordTxtFld.text!] as [String : Any]
            Alamofire.request(getUserEditedDetailsURL + id, method: .put, parameters: updatedUserParams, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                SVProgressHUD.dismiss()
                self.blurView.isHidden = true
                print(response)
                switch(response.result) {
                case .success:
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "challengeListController") as! ChallengeListController
                    self.navigationController?.pushViewController(destVc, animated: true)
                    UserDefaults.standard.set(self.emailTxtFld.text!, forKey: "emailId")
                    UserDefaults.standard.synchronize()
                case .failure(_):
                    SVProgressHUD.dismiss()
                    self.blurView.isHidden = true
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            SVProgressHUD.dismiss()
            print("no internet")
            alertBox(myMsg: "no internet connection")
        }
    }
    @IBAction func userProfileTapped(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        userProfileImage.contentMode = .scaleAspectFit
        selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        if let selectedImages = selectedImage {
         SVProgressHUD.show(withStatus: "Please wait..")
            self.blurView.isHidden = false
            if NetworkingManager.isConnectedToNetwork(){
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(UIImageJPEGRepresentation(selectedImages,0.3)!, withName: "adduserform", fileName: "swift_file.jpeg", mimeType: "image/jpg/jpeg/png")
                }, to:"\(uploadProfileURL)\(emailTxtFld.text!)/true"){ (result) in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            SVProgressHUD.dismiss()
                            print(response)
                            self.blurView.isHidden = true
                            self.userProfileImage.image = selectedImages
                            self.alertBox(myMsg: "Profile Pic uploaded succesfully")
                            debugPrint(response)
                        }
                    case .failure(let encodingError):
                        SVProgressHUD.dismiss()
                        self.blurView.isHidden = true
                        print(encodingError)
                    }
                }
            }else{
                SVProgressHUD.dismiss()
                print("no internet")
                alertBox(myMsg: "no internet connection")
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    @IBAction func editBtnTapped(_ sender: Any) {
        saveBtn.isHidden = false
        emailTxtFld.isUserInteractionEnabled = true
        passwordTxtFld.isUserInteractionEnabled = true
        mobileTxtFld.isUserInteractionEnabled = true
        userNameTxtFld.isUserInteractionEnabled = true
        userProfileButton.isUserInteractionEnabled = true
    }
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func signOutTapped(_ sender: Any){
        if NetworkingManager.isConnectedToNetwork(){
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(deleteTokenURL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                print(response)
                switch (response.result){
                case .success(_):
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "loginController") as! LoginController
                    self.navigationController?.pushViewController(destVc, animated: true)
                    UserDefaults.standard.set("", forKey: "token")
                    UserDefaults.standard.synchronize()
                    break
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
            alertBox(myMsg: "no internet connection")
        }
    }
//    {
//        let destVc = storyboard?.instantiateViewController(withIdentifier: "loginController") as! LoginController
//        navigationController?.pushViewController(destVc, animated: true)
//        UserDefaults.standard.set("", forKey: "token")
//        UserDefaults.standard.synchronize()
//    }
    @IBAction func backBtnTapped(_ sender: Any) {
        performSegueToReturnBack()
    }
}
