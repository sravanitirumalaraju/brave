//
//  WebViewController.swift
//  Brave
//
//  Created by apple on 16/02/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class WebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
     var receiveURL = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        webView.scrollView.bounces = false
        webView.delegate = self
        showWebView(webURL: receiveURL)
    }
    func showWebView(webURL: String){
        let url = NSURL(string: webURL)
        let requestUrl = URLRequest(url: url! as URL)
        webView.scalesPageToFit = true
        webView.loadRequest(requestUrl)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backClicked(_ sender: Any) {
        SVProgressHUD.dismiss()
        webView.stopLoading()
        performSegueToReturnBack()
    }
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        SVProgressHUD.dismiss()
    }
}
